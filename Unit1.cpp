//##############################################################################
//                      EARTH ORBIT MODEL
//##############################################################################
//==============================================================================

//##### ..... #####
#include	<vcl.h>
#pragma	hdrstop	//--------------------------------------------------------------
#include	"Unit1.h"
#include <iostream>
#include <fstream>
#include <cmath>	// .........

//------------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//------------------------------------------------------------------------------


//##### ..... #####
// ����� ����� ���������� ����������� �������� ������.
const int	steps_num = 60;
// ��������� ������� ������.
bool	orbits_state = false;
bool    axis_state = false;
bool    value_state = true;
bool    rv_state = false;

const double GM = 398600.4415;
const double pi = 3.1415926535;
const double t = 0;
const double fiiii = 0;

double kordinat_sputnik_x (double a, double vRad, double t, double f){
	double gh_x = a*cos(vRad*t + f);

	return gh_x;
}

double kordinat_sputnik_y (double b, double vRad, double t, double f){
	double gh_y = b*sin(vRad*t + f);

	return gh_y;
}
	double xgr = 80;
	double v = 7;
	double x_angle = (xgr/180)*pi;

	double r = 9000;

	double a = r/(2 - ((pow(v,2)*r)/GM));

	double e = sqrt(1 - ((pow(v,2)*pow(r,2))/(a*GM))*pow(sin(x_angle),2));

	double b = a*sqrt(1-pow(e,2));

	double c = a*e;

	double p = a*(1 - pow(e,2));

	double avgv = sqrt(GM/a);

	double rP = a*(1-e);

	double rA = a*(1+e);

	double vP = avgv*sqrt((1-e)/(1+e));

	double vA = avgv*sqrt((1+e)/(1-e));

	double T = 2*pi*sqrt(pow(a,3)/GM);

	double S = pi*a*b;

	double isanom = acos((1/e)*((a*(1-pow(e,2)))/r-1));

	double eanom = acos((e+cos(isanom))/(1+e*cos(isanom)));

	double avganom = eanom - e*sin(eanom);

	double deltat = (avganom*T)/(2*pi);

	double rdop = 2*a-r;

	double vRad = (pi*2)/T;

	double kord_earth_x = a - rP;
	double kord_earth_y = 0;

	double gh_x = kordinat_sputnik_x(a,vRad,t,fiiii);
	double gh_y = kordinat_sputnik_y(b,vRad,t,fiiii);

     double i_x,i_y;

//##### ..... #####
// �����������.
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	// ������� �������� �����.
	Width = 1900;
	Height = 800;
	// �������������� �������.
	DoubleBuffered = true;
	// ���� �����.
	Form1->Color = clBlack;	//(clGray)
	// ������������� ������� ����. �����.
	randomize();

	// *** ������� B_Orbits. ***
	// �������.
	B_Orbits->Width = 60;
	B_Orbits->Height = 20;
	// ���������.
	B_Orbits->Top = ClientHeight - B_Orbits->Height - 5;
	B_Orbits->Left = 10;

	// *** ������� B_Axis. ***
	// �������.
	B_Axis->Width = 60;
	B_Axis->Height = 20;
	// ���������.
	B_Axis->Top =  B_Orbits->Top - 30;
	B_Axis->Left = 10;

	// *** ������� B_Values. ***
	// �������.
	B_Values->Width = 60;
	B_Values->Height = 20;
	// ���������.
	B_Values->Top =  B_Axis->Top - 30;
	B_Values->Left = 10;

	// *** ������� B_RV. ***
	// �������.
	B_RV->Width = 60;
	B_RV->Height = 20;
	// ���������.
	B_RV->Top =  B_Values->Top - 30;
	B_RV->Left = 10;


	// ��������� �������.
	Timer1->Interval = 30;
	Timer1->Enabled = true;

	// ���������� ������ ����������� (�����).
	x_centr = ClientWidth/2;	//(int)
	y_centr = ClientHeight/2;	//(int)

	// ������ ��������� ��������� ���� �����.

	f[0] =3.0;	//(double)


	// ������ ����� ���� �����.
	clrs[0] = clBlue;	//(TColor)

	// ������ ������� (�������) ���� �����.
	rads[0] = 4;	//(double)

	// ������ �������� ���� �����.
	speeds[0] = vRad*10;	//(double)

	// ���������������� ������� ��������� ����������.
	m_start_cnt = 0;
	// ���������� ������������ ������.
	m_start = false;

	// *** ������������� ����. �����. ***
	// �� ���� ����. �������.
	for ( int s=0; s<STS_MAX; s++ )
	{
		// *** ����. ������ � �������� ���������. ***
		sts_act[s] = false;	// ��������� ����. ������ - �� �������.	//(bool)
		sts_num[s] = 0;	// ���� �������� ��������� ������ - ���������.	//(int)
	}

	if (value_state)
	{
	L_A->Caption = "Semi-major axis = " + FloatToStrF(a, ffFixed, 10, 0) + " km";
		L_B->Caption = "Semi-minor axis = " + FloatToStrF(b, ffFixed, 10, 0) + " km";
		L_C->Caption = "Distance to Earth = " + FloatToStrF((a-c-6300), ffFixed, 10, 0) + " km";
		L_V->Caption = "AVG V = " + FloatToStrF(avgv, ffFixed, 10, 3) + " km/s";
		L_VPVA->Caption = "min V = " + FloatToStrF(vP, ffFixed, 10, 1) + " km/s" + "    " + "max V = " + FloatToStrF(vA, ffFixed, 10, 0) + " km/s";
		L_Time->Caption = "T = " + FloatToStrF(T, ffFixed, 10, 0) + " s" + "  =  " + FloatToStrF((T/3600), ffFixed, 10, 1) + " h";
		L_ianom->Caption = "True anomaly = " + FloatToStrF((isanom*pi), ffFixed, 10, 2) + "�";
		L_eanom->Caption = "Eccentric anomaly = " + FloatToStrF((eanom*pi), ffFixed, 10, 2) + "�";
		L_avganom->Caption = "Mean anomaly = " + FloatToStrF((avganom*pi), ffFixed, 10, 2) + "�";
		L_Vrad->Caption = "Angular Velocity = " + FloatToStrF(vRad, ffFixed, 10, 5) + " �/s" + "  =  " + FloatToStrF((vRad*86400), ffFixed, 10, 2) + " �/day";
	}


}


//##############################################################################
// .........
void __fastcall		TForm1::Timer1Timer(TObject *Sender)
{

	// �� ���� ������.
	for ( int	p=0; p<PNTS_MAX; p++ )
	{
		// ���������� ���������� ��������� �����.
		f[p] += speeds[p];
		if ( f[p] >= 2.0*M_PI )	// �������� �� ���������� ������� ����� (��� ��).
		{
			f[p] = 0.0;	//(double)	// ������ ����� ����.
		}

		// ��������� � ��������� ���������� ��������� �����.
		pnts[p].x = x_centr + (a/200)*cos( f[p] );
		pnts[p].y = y_centr + (b/200)*sin( f[p] );

		if (rv_state)
		 {
			//��������� ������-�������
			double earth_x = x_centr - (kord_earth_x/200);
			double earth_y = y_centr;

			Canvas->Pen->Color = clRed;
			Canvas->MoveTo(earth_x,earth_y);
			Canvas->LineTo(pnts[p].x,pnts[p].y);
		 }
	}
	//--------------------------------------------------------------------------
	// ��������� ������� ��������� ����������.
	m_start_cnt++;
	if ( m_start_cnt >= 1000 )	// ���� ���� �������� ��������� ...
	{
		m_start_cnt = 0;	// �������� ������� ��������� ����������.
		m_start = true;	// ��������� ���������.

		// *** ������������� ������������ ������. ***
		// ������ ���������� ������ ���������� (���������).
//+		x = 20;					// ���������� ����� (�����).
//+		x = ClientWidth - 20;	// ���������� ����� (������).
		x = ClientWidth - rand()%ClientWidth;	// ��������.
		y = 0;
		x_st = (x_centr - x)/steps_num;	// ���������� ���� ����������� ����������.
		y_st = y_centr/steps_num;
		// ������ ����� �����. ����������.
		m_num = 2 + rand()%(MTS_MAX-2) + 1;	//(����������� � ��������: 3, ..., MTS_MAX)
//		m_num = 10;
		// ���������������� ���������� ���������. ���������� (������ ���������, ��������� �������).
		for ( int	i=0; i<m_num; i++ )
		{
			xx[i] = x + rand()%40;
			yy[i] = y - rand()%30;	//(20)
		}
	}
	//--------------------------------------------------------------------------
	// �� ���� �������.
	for ( int	s=0; s<STS_MAX; s++ )
	{
		// ���� ��������� ������ - �� �������� ...
		if ( !sts_act[s] )
		{
			// *** �������������� ��������� ������. ***
			sts[s].x = rand()%ClientWidth;	// ���������������� ���������� ��������� ����. ������.
			sts[s].y = rand()%ClientHeight;
			sts_num[s] = 0;	// ���� ���������� ��������� ����. ������ - ���������.
			sts_act[s] = true;	// ��������� ������ ������ ����� �������.
		}
	}
	//--------------------------------------------------------------------------
	// ������������ �����.
	Repaint();
}

//##############################################################################
// ������ ����������� ����� (� ����.).
const double	R = 30.0;

// .........
void __fastcall		TForm1::FormPaint(TObject *Sender)
{
/*	//#####
	// *** ��������� ����. ***
	// ��������� ����.
	Canvas->Pen->Width = 1;
	Canvas->Pen->Color = clBlue;
	// �������������� ���.
	Canvas->MoveTo( x_centr-270, y_centr );	//(int, int)
	Canvas->LineTo( x_centr+270, y_centr );	//(int, int)
	// ������������ ���.
	Canvas->MoveTo( x_centr, y_centr-120 );	//(int, int)
	Canvas->LineTo( x_centr, y_centr+120 );	//(int, int)
*/	//#####

	// *** ��������� �����. ***
	// ��������� ���� � �����.
	Canvas->Pen->Width = 1;
	Canvas->Pen->Color = clBlue;
	Canvas->Brush->Color = clGreen;	//clWhite;
	// ��������� �����.
	Canvas->Ellipse(
		x_centr - (kord_earth_x/200) - R, y_centr - R,	//(int, int,)
		x_centr - (kord_earth_x/200) + R, y_centr + R );	//(int, int)

	//--------------------------------------------------------------------------
	// ���� ��������� ����� - ��������� ...
	if ( orbits_state )
		// *** ��������� ����� ���� ������. ***
		// �� ���� ��������.
		for ( int	p=0; p<PNTS_MAX; p++ )
		{
			// �� ���� ������ (� �������� �����).
			for ( double	f=0.0; f<2.0*M_PI; f+=0.02 )
			{
				// ������� ������� ����� (������������ ���� �������).
				Canvas->Pixels[x_centr + (a/200)*cos( f )][y_centr + (b/200)*sin( f )] = clrs[p];
			}
		}

		if (axis_state) {

			for (double i = 0; i < (2*(a/200)); i++)
			{
				Canvas->Pixels[x_centr - (a/200) + i][y_centr] = clWhite;
			}

			for (double i = 0; i < (2*(b/200)); i++)
			{
				Canvas->Pixels[x_centr][y_centr - (b/200) + i] = clWhite;
			}
		}

	//--------------------------------------------------------------------------

	// *** ��������� ���� �����. ***
	// �� ���� ������.
	for ( int	p=0; p<PNTS_MAX; p++ )
	{
		// ��������� ���� � �����.
		Canvas->Pen->Width = 1;
		Canvas->Pen->Color = clrs[p];
		Canvas->Brush->Color = clrs[p];

		// �������� ������ ��������� �����.
		double	r = rads[p];
		// ���������� ���� ��������� �����.
		Canvas->Ellipse(
			pnts[p].x - r, pnts[p].y - r,	//(int, int,)
			pnts[p].x + r, pnts[p].y + r );	//(int, int)
	}
	//--------------------------------------------------------------------------
	// *** ��������� ����������. ***
	// ���� ����� ���������� - ��������� ...
	if ( m_start )
	{
		// ��������� ���� � �����.
		Canvas->Pen->Width = 1;
		Canvas->Pen->Color = clWhite;

		// ��������� ����.
		Canvas->MoveTo( x, y );	//(int, int)
		// ������� ��� �� �����������.
		x += x_st;
		y += y_st;
		// �������� ����.
		Canvas->LineTo( x, y );	//(int, int)

		// ���������� ���������. ���������.
		for ( int	i=0; i<m_num; i++ )
		{
			// ��������� ����.
			Canvas->MoveTo( xx[i], yy[i] );	//(int, int)
			// ������� ��� �� �����������.
			xx[i] += x_st;
			yy[i] += y_st;
			// �������� ����.
			Canvas->LineTo( xx[i], yy[i] );	//(int, int)
		}

		// ���� ����� �� ��� ������ ... ��������� ����� ����������.
		if ( y >= y_centr*3 )	m_start = false;
	}
	//--------------------------------------------------------------------------
	// *** ��������� �������� �����. ***
	// �� ���� ��������� �������.
	for ( int	s=0; s<STS_MAX; s++ )
	{
		// ���� ��������� ������ - ������� ...
		if ( sts_act[s] )
		{
			//(������ ������ ����� ��� ���� ����������: 0, 2 - �����; 1 - �������.)
			if ( sts_num[s] == 0	|| sts_num[s] == 2 )
			{
				// ���������� ������� ������ (����� ����������).
				Canvas->Pixels[sts[s].x][sts[s].y] = clrs[rand()%PNTS_MAX];	// ���������� ����� ������.
			}
			else if (sts_num[s] == 1 )
			{
				// ���������� ����� ����� ������ (������� ����������).
				Canvas->Pixels[sts[s].x][sts[s].y] = clWhite;
			}
			if ( sts_num[s] < 2 )	// ���� ���� ������ 3 ���� ���������� ...
				sts_num[s]++;		// ������� � ��������� ���� ����������.
			else	// ���� ����� �� ������ 3-� ���� ���������� ...
			{
				// *** ����. ������ � �������� ���������. ***
				sts_num[s] = 0;		// �������� ������� ��� ����������.
				sts_act[s] = false;	// ��������� ������ - ��������� ���� ����������.
			}
		}
	}
	//--------------------------------------------------------------------------
}

//##############################################################################
// .........
void __fastcall		TForm1::B_OrbitsClick(TObject *Sender)
{
	if ( orbits_state )	// ���� ������� - �������� ...
	{
		// ��������� �������.
		orbits_state = false;
	}
	else	// ���� ������� - ��������� ...
	{
		// �������� �������.
		orbits_state = true;
	}
}


//##############################################################################



//		   using namespace System;

void __fastcall TForm1::Button1Click(TObject *Sender)
{

  // double x1;

   xgr = E_Angle->Text.ToDouble();

   x_angle = (xgr/180)*pi;

   r = E_Radius->Text.ToDouble();
   v = E_Speed->Text.ToDouble();


	a = r/(2 - ((pow(v,2)*r)/GM));

 //	x1 = x;

	e = sqrt(1 - ((pow(v,2)*pow(r,2))/(a*GM))*pow(sin(x_angle),2));

	b = a*sqrt(1-pow(e,2));

	c = a*e;

	p = a*(1 - pow(e,2));

	avgv = sqrt(GM/a);

	rP = a*(1-e);

	rA = a*(1+e);

	vP = avgv*sqrt((1-e)/(1+e));

	vA = avgv*sqrt((1+e)/(1-e));

	T = 2*pi*sqrt(pow(a,3)/GM);

	S = pi*a*b;

	isanom = acos((1/e)*((a*(1-pow(e,2)))/r-1));

	eanom = acos((e+cos(isanom))/(1+e*cos(isanom)));

	avganom = eanom - e*sin(eanom);

	deltat = (avganom*T)/(2*pi);

	rdop = 2*a-r;

	vRad = (pi*2)/T;

	kord_earth_x = a - rP;
	kord_earth_y = 0;

	gh_x = kordinat_sputnik_x(a,vRad,t,fiiii);
	gh_y = kordinat_sputnik_y(b,vRad,t,fiiii);

		if (value_state)
	{
		L_A->Caption = "Semi-major axis = " + FloatToStrF(a, ffFixed, 10, 0) + " km";
		L_B->Caption = "Semi-minor axis = " + FloatToStrF(b, ffFixed, 10, 0) + " km";
		L_C->Caption = "Distance to Earth = " + FloatToStrF((a-c-6300), ffFixed, 10, 0) + " km";
		L_V->Caption = "AVG V = " + FloatToStrF(avgv, ffFixed, 10, 3) + " km/s";
		L_VPVA->Caption = "min V = " + FloatToStrF(vP, ffFixed, 10, 1) + " km/s" + "    " + "max V = " + FloatToStrF(vA, ffFixed, 10, 0) + " km/s";
		L_Time->Caption = "T = " + FloatToStrF(T, ffFixed, 10, 0) + " s" + "  =  " + FloatToStrF((T/3600), ffFixed, 10, 1) + " h";
		L_ianom->Caption = "True anomaly = " + FloatToStrF((isanom*pi), ffFixed, 10, 2) + "�";
		L_eanom->Caption = "Eccentric anomaly = " + FloatToStrF((eanom*pi), ffFixed, 10, 2) + "�";
		L_avganom->Caption = "Mean anomaly = " + FloatToStrF((avganom*pi), ffFixed, 10, 2) + "�";
		L_Vrad->Caption = "Angular Velocity = " + FloatToStrF(vRad, ffFixed, 10, 5) + " �/s" + "  =  " + FloatToStrF((vRad*86400), ffFixed, 10, 2) + " �/day";
	}

}
//---------------------------------------------------------------------------


void __fastcall TForm1::B_AxisClick(TObject *Sender)
{
      if ( axis_state )	// ���� ������� - �������� ...
	{
		// ��������� �������.
		axis_state = false;
	}
	else	// ���� ������� - ��������� ...
	{
		// �������� �������.
		axis_state = true;
	}
}

//---------------------------------------------------------------------------


void __fastcall TForm1::B_ValuesClick(TObject *Sender)
{
if ( value_state )	// ���� ������� - �������� ...
	{
		// ��������� �������.
		value_state = false;
	}
	else	// ���� ������� - ��������� ...
	{
		// �������� �������.
		value_state = true;
	}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::B_RVClick(TObject *Sender)
{
   if ( rv_state )	// ���� ������� - �������� ...
	{
		// ��������� �������.
		rv_state = false;
	}
	else	// ���� ������� - ��������� ...
	{
		// �������� �������.
		rv_state = true;
	}
}
//---------------------------------------------------------------------------

