object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 569
  ClientWidth = 644
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 92
    Height = 20
    Caption = #1057#1082#1088#1086#1089#1090#1100' ('#1082#1084'/'#1089'):'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
  end
  object Label2: TLabel
    Left = 8
    Top = 34
    Width = 97
    Height = 20
    Caption = #1059#1075#1086#1083' ('#1075#1088#1072#1076#1091#1089#1099'):'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
  end
  object Label3: TLabel
    Left = 8
    Top = 60
    Width = 122
    Height = 20
    Caption = #1056#1072#1076#1080#1091#1089' '#1074#1077#1082#1090#1086#1088' ('#1082#1084'):'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
  end
  object L_A: TLabel
    Left = 8
    Top = 104
    Width = 21
    Height = 20
    Caption = 'L_A'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object L_B: TLabel
    Left = 8
    Top = 130
    Width = 22
    Height = 20
    Caption = 'L_B'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object L_C: TLabel
    Left = 8
    Top = 156
    Width = 23
    Height = 20
    Caption = 'L_C'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object L_V: TLabel
    Left = 8
    Top = 182
    Width = 21
    Height = 20
    Caption = 'L_V'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object L_VPVA: TLabel
    Left = 8
    Top = 216
    Width = 39
    Height = 20
    Caption = 'L_V{VA'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object L_Time: TLabel
    Left = 8
    Top = 272
    Width = 44
    Height = 20
    Caption = 'L_Time'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object L_ianom: TLabel
    Left = 8
    Top = 298
    Width = 49
    Height = 20
    Caption = 'L_ianom'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object L_eanom: TLabel
    Left = 8
    Top = 324
    Width = 53
    Height = 20
    Caption = 'L_eanom'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object L_avganom: TLabel
    Left = 8
    Top = 350
    Width = 65
    Height = 20
    Caption = 'L_avganom'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object L_Vrad: TLabel
    Left = 8
    Top = 242
    Width = 39
    Height = 20
    Caption = 'L_Vrad'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object B_Orbits: TButton
    Left = 8
    Top = 536
    Width = 75
    Height = 25
    Caption = 'Orbits'
    TabOrder = 0
    OnClick = B_OrbitsClick
  end
  object E_Speed: TEdit
    Left = 152
    Top = 10
    Width = 65
    Height = 21
    TabOrder = 1
    Text = '7'
  end
  object E_Angle: TEdit
    Left = 152
    Top = 37
    Width = 65
    Height = 21
    TabOrder = 2
    Text = '80'
  end
  object E_Radius: TEdit
    Left = 152
    Top = 64
    Width = 65
    Height = 21
    TabOrder = 3
    Text = '9000'
  end
  object Button1: TButton
    Left = 223
    Top = 34
    Width = 75
    Height = 25
    Caption = #1054#1090#1086#1073#1088#1072#1079#1080#1090#1100
    TabOrder = 4
    OnClick = Button1Click
  end
  object B_Axis: TButton
    Left = 8
    Top = 505
    Width = 75
    Height = 25
    Caption = 'Axis'
    TabOrder = 5
    OnClick = B_AxisClick
  end
  object B_Values: TButton
    Left = 8
    Top = 474
    Width = 75
    Height = 25
    Caption = 'Values'
    TabOrder = 6
    OnClick = B_ValuesClick
  end
  object B_RV: TButton
    Left = 8
    Top = 443
    Width = 75
    Height = 25
    Caption = 'Radius-Vector'
    TabOrder = 7
    OnClick = B_RVClick
  end
  object Timer1: TTimer
    Interval = 2000
    OnTimer = Timer1Timer
    Left = 608
    Top = 8
  end
end
