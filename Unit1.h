//##############################################################################
// <<< Unit1.h >>>
//##############################################################################
// ��������: - http://www.cyberforum.ru/cpp-builder/thread1761351.html  (������.)
//==============================================================================
#ifndef		Unit1H
#define		Unit1H

//------------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//------------------------------------------------------------------------------


//##### ..... #####
// ������������ ����� �����.
const int	PNTS_MAX = 1;
// ����� �������� ���������� � ���������.
const int	MTS_MAX = 4;	//(10)
// ����� ������������ ��������� �����.
const int	STS_MAX = 5;


//==============================================================================
//  ������� ��������:
//    ����� ������ - PNTS_MAX.
//    ����� ����������: - ���� �������� �������� (�� ������ ����������� ��������
// ������������ ������), ���� ��������������� ���������. ����� ���������������
// ���������� (m_num) �������� ��� ������� ������������ ������ �������� ���������
// ����� � �������� 3 - MTS_MAX.
//    ��� ����������� ��������� ����� (������������ �������������� STS_MAX �����)
// ������ ������ ��������������� ���� ���������� �� ������� � 0-� ���� ��������.
// � �������� �������� ������ ������ �������� ��� ���� 0-� � 2-� - ������ ��������;
// � 1-� - ������� ��������.
//
//==============================================================================


//##### ..... #####
// .........
class	TForm1	: public	TForm
{

__published:	// IDE-managed Components
	TTimer *Timer1;
	TButton *B_Orbits;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TEdit *E_Speed;
	TEdit *E_Angle;
	TEdit *E_Radius;
	TButton *Button1;
	TButton *B_Axis;
	TLabel *L_A;
	TLabel *L_B;
	TLabel *L_C;
	TLabel *L_V;
	TLabel *L_VPVA;
	TLabel *L_Time;
	TLabel *L_ianom;
	TLabel *L_eanom;
	TLabel *L_avganom;
	TButton *B_Values;
	TButton *B_RV;
	TLabel *L_Vrad;
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall FormPaint(TObject *Sender);
	void __fastcall B_OrbitsClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall B_AxisClick(TObject *Sender);
	void __fastcall B_ValuesClick(TObject *Sender);
	void __fastcall B_RVClick(TObject *Sender);

private:	// User declarations
	// *** �������. ***
	// ��������� �������� ���� �����.
	//double	a[PNTS_MAX], b[PNTS_MAX];
	// ����� ���� �����.
	TColor	clrs[PNTS_MAX];
	// ������� ���� ����� (�������).
	double	rads[PNTS_MAX];
	// �������� �������� ���� �����.
	double	speeds[PNTS_MAX];
	// ���������� ���� ����� �� ������� (���������, � ����.).
	TPoint	pnts[PNTS_MAX];
	// ������� ���������� ���� ����� �� ������� (� ��������).
	double	f[PNTS_MAX];
	// ���������� ������ ����������� (�����)(� ����.).
	int		x_centr, y_centr;

	// *** ����������� �����. ***
	// ���������� ������������ ������.
	bool	m_start;
	// ������� ��������� ������������ ������.
	int		m_start_cnt;
	// ���������� ��������� (���������).
	int		x, y;
	// ���� ���������� ��������� ��������� (���������).
	double	x_st, y_st;
	// ����� ���������. ���������� (���������� ����. �������)(m_num <= MTS_MAX).
	int		m_num;
	// ���������� ���������. ����������.
	int		xx[MTS_MAX];
	int		yy[MTS_MAX];

	// *** �������� �����. ***
	// ���������� ���� ����. �����.
	TPoint	sts[STS_MAX];
	// �������� ���������� ���� ����. �����.
	bool	sts_act[STS_MAX];
	// �������� ��� ���������� ���� ����. ����� (�������: 0, 1, 2.).
	int		sts_num[STS_MAX];

public:		// User declarations
	// �����������.
	__fastcall	TForm1(TComponent* Owner);

};
//------------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//------------------------------------------------------------------------------

#endif	//(Unit1H)
//##############################################################################

